package com.ctetin.expandabletextviewlibrary;

import com.ctetin.expandabletextviewlibrary.app.LinkType;
import com.ctetin.expandabletextviewlibrary.app.StatusType;
import com.ctetin.expandabletextviewlibrary.model.ExpandableStatusFix;
import com.ctetin.expandabletextviewlibrary.model.FormatData;
import com.ctetin.expandabletextviewlibrary.model.UUIDUtils;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ElementScatter;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.text.RichText;
import ohos.agp.text.RichTextBuilder;
import ohos.agp.text.RichTextLayout;
import ohos.agp.text.TextForm;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.app.Context;
import ohos.miscservices.inputmethod.RichContent;
import ohos.multimodalinput.event.TouchEvent;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class ExpandableTextView extends Text implements Component.BindStateChangedListener {
  private static final int DEF_MAX_LINE = 4;
  public static final String TEXT_CONTRACT = "收起";
  public static final String TEXT_EXPEND = "展开";
  public static final String Space = "  ";
  public static final String TEXT_TARGET = "网页链接";
  public static final String IMAGE_TARGET = "图";
  public static final String TARGET = IMAGE_TARGET + TEXT_TARGET;
  public static final String DEFAULT_CONTENT = "                                                 " +
      "                                                                                          " +
      "                                                                                          " +
      "                                                                                      ";

  private static int retryTime = 0;

  public static final String regexp_mention = "@[\\w\\p{InCJKUnifiedIdeographs}-]{1,26}";
  public static final String self_regex = "\\[([^\\[]*)\\]\\(([^\\(]*)\\)";

  private Paint mPaint;

  /* 记录当前Model */
  private ExpandableStatusFix mModel;

  private RichTextLayout mDynamicLayout;

  // HIDE 状态下，展示多少行开始省略
  private int mLimitLines;

  private int currentLines;

  private int mWidth;

  /* 链接和@用户的事件点击 */
  private OnLinkClickListener linkClickListener;

  /* 点击展开或者收回按钮的时候 是否真的执行操作 */
  private boolean needRealExpandOrContract = true;

  /* 展开或者收回事件监听 */
  private OnExpandOrContractClickListener expandOrContractClickListener;

  /* 是否需要收起 */
  private boolean mNeedContract = true;

  private FormatData mFormatData;

  // 是否需要展开功能
  private boolean mNeedExpend = true;

  // 是否需要转换url成网页链接四个字
  private boolean mNeedConvertUrl = true;

  // 是否需要@用户的功能
  private boolean mNeedMention = true;

  // 是否需要对链接进行处理
  private boolean mNeedLink = true;

  // 是否需要对自定义情况进行处理
  private boolean mNeedSelf = false;

  // 是否需要永远将展开或收回显示在最右边
  private boolean mNeedAlwaysShowRight = false;

  // 是否需要动画 默认开启动画
  private boolean mNeedAnimation = true;

  private int mLineCount;

  private CharSequence mContent;

  /**
   * 展开文字的颜色
   */
  private Color mExpandTextColor;
  /**
   * 展开文字的颜色
   */
  private Color mMentionTextColor;

  /**
   * 链接的字体颜色
   */
  private Color mLinkTextColor;

  /**
   * 自定义规则的字体颜色
   */
  private Color mSelfTextColor;

  /**
   * 收起的文字的颜色
   */
  private Color mContractTextColor;

  /**
   * 展开的文案
   */
  private String mExpandString;
  /**
   * 收起的文案
   */
  private String mContractString;

  /**
   * 在收回和展开前面添加的内容
   */
  private String mEndExpandContent;

  /**
   * 在收回和展开前面添加的内容的字体颜色
   */
  private Color mEndExpandTextColor;

  // 是否AttachedToWindow
  private boolean isAttached;

  public ExpandableTextView(Context context, AttrSet attrSet, String styleName) {
    super(context, attrSet, styleName);
  }

  public ExpandableTextView(Context context, AttrSet attrSet) {
    super(context, attrSet);
    initAttr(context, attrSet);
    setBindStateChangedListener(this);
  }

  public ExpandableTextView(Context context) {
    super(context);
  }

  @Override
  public void onComponentBoundToWindow(Component component) {
    if (isAttached == false) doSetContent();
    isAttached = true;
  }

  @Override
  public void onComponentUnboundFromWindow(Component component) {

  }

  private void initAttr(Context context, AttrSet attrSet) {
    // 适配英文版（暂时未配置）

    if (attrSet != null) {
      mLimitLines = attrSet.getAttr("ep_max_line").isPresent() ?
          attrSet.getAttr("ep_max_line").get().getIntegerValue() : DEF_MAX_LINE;
      mNeedExpend = attrSet.getAttr("ep_need_expand").isPresent() ? attrSet.getAttr(
          "ep_need_expand").get().getBoolValue() : true;
      mNeedContract = attrSet.getAttr("ep_need_contract").isPresent() ? attrSet.getAttr(
          "ep_need_contract").get().getBoolValue() : false;
      mNeedAnimation = attrSet.getAttr("ep_need_animation").isPresent() ? attrSet.getAttr(
          "ep_need_animation").get().getBoolValue() : true;
      mNeedSelf = attrSet.getAttr("ep_need_self").isPresent() ?
          attrSet.getAttr("ep_need_self").get().getBoolValue() : false;
      mNeedMention = attrSet.getAttr("ep_need_mention").isPresent() ? attrSet.getAttr(
          "ep_need_mention").get().getBoolValue() : true;
      mNeedLink = attrSet.getAttr("ep_need_link").isPresent() ?
          attrSet.getAttr("ep_need_link").get().getBoolValue() : true;
      mNeedAlwaysShowRight = attrSet.getAttr("ep_need_always_showright").isPresent() ?
          attrSet.getAttr("ep_need_always_showright").get().getBoolValue() : false;
      mNeedConvertUrl = attrSet.getAttr("ep_need_convert_url").isPresent() ? attrSet.getAttr(
          "ep_need_convert_url").get().getBoolValue() : true;
      mContractString = attrSet.getAttr("ep_contract_text").isPresent() ? attrSet.getAttr(
          "ep_contract_text").get().getStringValue() : TEXT_CONTRACT;
      mExpandString = attrSet.getAttr("ep_expand_text").isPresent() ? attrSet.getAttr(
          "ep_expand_text").get().getStringValue() : TEXT_EXPEND;
      mExpandTextColor = attrSet.getAttr("ep_expand_color").isPresent() ? attrSet.getAttr(
          "ep_expand_color").get().getColorValue() : new Color(0xFF999999);
      mEndExpandTextColor = attrSet.getAttr("ep_expand_color").isPresent() ? attrSet.getAttr(
          "ep_expand_color").get().getColorValue() : new Color(0xFF999999);
      mContractTextColor = attrSet.getAttr("ep_contract_color").isPresent() ? attrSet.getAttr(
          "ep_contract_color").get().getColorValue() : new Color(0xFF999999);
      mLinkTextColor = attrSet.getAttr("ep_link_color").isPresent() ? attrSet.getAttr(
          "ep_link_color").get().getColorValue() : new Color(0xFFFF6200);
      mSelfTextColor = attrSet.getAttr("ep_self_color").isPresent() ? attrSet.getAttr(
          "ep_self_color").get().getColorValue() : new Color(0xFFFF6200);
      mMentionTextColor = attrSet.getAttr("ep_mention_color").isPresent() ? attrSet.getAttr(
          "ep_mention_color").get().getColorValue() : new Color(0xFFFF6200);

//            int resId = attrSet.getAttr("ep_link_res").isPresent() ? attrSet.getAttr
//            ("ep_link_res").get().getIntegerValue() : ResourceTable.Media_link;
//            mLinkDrawable = ElementScatter.getInstance(context).parse(resId);
      currentLines = mLimitLines;
    } else {
//            try {
//                mLinkDrawable = ElementScatter.getInstance(context).parse(ResourceTable
//                .Media_link);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
    }

    mContext = context;

    // 暂时写法（用于替换TextView 的 getPaint()）
    mPaint = new Paint();
    mPaint.setTextSize(this.getTextSize());
    mPaint.setColor(this.getTextColor());
    mPaint.setMultipleLine(true);
    mPaint.setTextAlign(this.getTextAlignment());
    mPaint.setFont(this.getFont());

    // 初始化link的图片
    // mLinkDrawable.setBounds(0, 0, 30, 30); //必须设置图片大小，否则不显示
  }

  private void setRealContent(CharSequence content) {
    // 处理给定的数据
    mFormatData = formatData(content);
    // 用来计算内容的大小
    RichTextBuilder builder = new RichTextBuilder();
    builder.mergeForm(new TextForm().setTextSize(this.getTextSize()).setLineHeight(180).setTextColor(this.getTextColor().getValue()));
    builder.addText(mFormatData.getFormatedContent());
    RichText richText = builder.build();
    mDynamicLayout = new RichTextLayout(richText, mPaint, new Rect(0, 0, 0, 0), mWidth, true);
    // 获取行数
    mLineCount = mDynamicLayout.getLineCount();

    if (onGetLineCountListener != null) {
      onGetLineCountListener.onGetLineCount(mLineCount, mLineCount > mLimitLines);
    }

    if (!mNeedExpend || mLineCount <= mLimitLines) {
      //不需要展开功能 直接处理链接模块
      dealLink(mFormatData, false);
    } else {
      dealLink(mFormatData, true);
    }
  }

  /**
   * 设置追加的内容
   *
   * @param endExpendContent
   */
  public void setEndExpendContent(String endExpendContent) {
    this.mEndExpandContent = endExpendContent;
  }

  /**
   * 设置内容
   *
   * @param content
   */
  public void setContent(final String content) {
    mContent = content;
    if (isAttached) doSetContent();
  }

  /**
   * 实际设置内容的
   */
  private void doSetContent() {
    if (mContent == null) return;
    currentLines = mLimitLines;

    if (mWidth <= 0) {
      if (getWidth() > 0) mWidth = getWidth() - getPaddingLeft() - getPaddingRight();
    }

    if (mWidth <= 0) {
      if (retryTime > 10) {
        setText(DEFAULT_CONTENT);
      }
      // TODO: 不知该如何处理
//            this.post(new Runnable() {
//                @Override
//                public void run() {
//                    retryTime++;
//                    setContent(mContent.toString());
//                }
//            });
    } else {
      setRealContent(mContent);
    }
  }

  /**
   * 设置最后的收起文案
   *
   * @return
   */
  private String getExpandEndContent() {
    if (mEndExpandContent == null || mEndExpandContent.length() == 0) {
      return String.format(Locale.getDefault(), "  %s",
          mContractString);
    } else {
      return String.format(Locale.getDefault(), "  %s  %s",
          mEndExpandContent, mContractString);
    }
  }

  /**
   * 设置展开的文案
   *
   * @return
   */
  private String getHideEndContent() {
    if (mEndExpandContent == null || mEndExpandContent.length() == 0) {
      return String.format(Locale.getDefault(), mNeedAlwaysShowRight ? "  %s" : "...  %s",
          mExpandString);
    } else {
      return String.format(Locale.getDefault(), mNeedAlwaysShowRight ? "  %s  %s" : "...  %s  %s",
          mEndExpandContent, mExpandString);
    }
  }

  /**
   * 处理文字中的链接问题
   *
   * @param formatData
   * @param ignoreMore
   */
  private void dealLink(FormatData formatData, boolean ignoreMore) {
    RichTextBuilder richTextBuilder = new RichTextBuilder();
    if (mModel != null && mModel.getStatus() != null) {
      boolean isHide = false;
      if (mModel.getStatus() != null) {
        if (mModel.getStatus().equals(StatusType.STATUS_CONTRACT)) {
          // 收起
          isHide = true;
        } else {
          // 展开
          isHide = false;
        }
      }

      if (isHide) {
        currentLines = mLimitLines + ((mLineCount - mLimitLines));
      } else {
        if (mNeedContract) currentLines = mLimitLines;
      }
    }

    if (ignoreMore) {
      if (currentLines < mLineCount) {
        int index = currentLines - 1;
        int startPosition = mDynamicLayout.getBeginCharIndex(index);
        int endPosition = mDynamicLayout.getEndCharIndex(index);
        float lineWidth = mDynamicLayout.getLimitWidth(index);

        String endString = getHideEndContent();

        //计算原内容被截取的位置下标
        int fitPosition = getFitPosition(endString, endPosition, startPosition, lineWidth,
            mPaint.measureText(endString), 0);

        String substring = formatData.getFormatedContent().substring(0, fitPosition);
        if (substring.endsWith("\n")) {
          substring = substring.substring(0, substring.length() - "\n".length());
        }

        List<FormatData.PositionData> datas = getAllPositionData(formatData.getPositionDatas(),
            substring);

        // 在被截断的文字后面添加 展开 文字
        substring += endString;
        mFormatData.setFormatedContent(substring);

        datas.add(new FormatData.PositionData(substring.length() - endString.length(),
            substring.length(), endString, LinkType.OPEN));
        mFormatData.setPositionDatas(datas);
      } else {
        if (mNeedContract) {
          String substring = formatData.getFormatedContent();
          String endString = getExpandEndContent();

          List<FormatData.PositionData> datas = getAllPositionData(mFormatData.getPositionDatas()
              , substring);

          if (mNeedAlwaysShowRight) {
            //计算一下最后一行有没有充满
            int index = mDynamicLayout.getLineCount() - 1;
            float lineWidth = mDynamicLayout.getLimitWidth(index);
            float lastLineWidth = 0;
            for (int i = 0; i < index; i++) {
              lastLineWidth += mDynamicLayout.getLimitWidth(i);
            }
            lastLineWidth = lastLineWidth / (index);
            float emptyWidth = lastLineWidth - lineWidth - mPaint.measureText(endString);
            if (emptyWidth > 0) {
              float measureText = mPaint.measureText(Space);
              int count = 0;
              while (measureText * count < emptyWidth) {
                count++;
              }
              count = count - 1;
              String spaceStr = "";
              for (int i = 0; i < count; i++) {
                spaceStr += Space;
              }
              substring += spaceStr;
              datas.add(new FormatData.PositionData(substring.length() - spaceStr.length(),
                  substring.length(), spaceStr, LinkType.DEFAULT));
            }
          }

          substring += endString;
          mFormatData.setFormatedContent(substring);

          datas.add(new FormatData.PositionData(substring.length() - endString.length(),
              substring.length(), endString, LinkType.CLOSE));
          mFormatData.setPositionDatas(datas);
        } else {
          if (!(mEndExpandContent == null || mEndExpandContent.length() == 0)) {
            String substring = formatData.getFormatedContent();
            List<FormatData.PositionData> datas =
                getAllPositionData(mFormatData.getPositionDatas(), substring);
            substring += mEndExpandContent;
            mFormatData.setFormatedContent(substring);

            datas.add(new FormatData.PositionData(substring.length() - mEndExpandContent.length()
                , substring.length(), mEndExpandContent, LinkType.EndExpand));
            mFormatData.setPositionDatas(datas);
          } else {
            String substring = formatData.getFormatedContent();
            List<FormatData.PositionData> datas =
                getAllPositionData(mFormatData.getPositionDatas(), substring);
            mFormatData.setPositionDatas(datas);
          }
        }
      }
    } else {
      if (!(mEndExpandContent == null || mEndExpandContent.length() == 0)) {
        String substring = formatData.getFormatedContent();
        substring += mEndExpandContent;
        mFormatData.setFormatedContent(substring);

        List<FormatData.PositionData> datas = mFormatData.getPositionDatas();
        datas.add(new FormatData.PositionData(substring.length() - mEndExpandContent.length(),
            substring.length(), mEndExpandContent, LinkType.EndExpand));
        mFormatData.setPositionDatas(datas);
      }
    }

    // 处理链接或者@用户
    List<FormatData.PositionData> positionDatas = formatData.getPositionDatas();
    for (FormatData.PositionData data : positionDatas) {
      int colorValue = this.getTextColor().getValue();
      if (data.getType() == LinkType.LINK_TYPE) {
        colorValue = mLinkTextColor.getValue();
      } else if (data.getType() == LinkType.MENTION_TYPE) {
        colorValue = mMentionTextColor.getValue();
      } else if (data.getType() == LinkType.SELF) {
        colorValue = mSelfTextColor.getValue();
      } else if (data.getType() == LinkType.OPEN) {
        colorValue = mContractTextColor.getValue();
      } else if (data.getType() == LinkType.CLOSE) {
        colorValue = mContractTextColor.getValue();
      } else if (data.getType() == LinkType.EndExpand) {
        colorValue = mExpandTextColor.getValue();
      }

      richTextBuilder.mergeForm(new TextForm().setTextSize(this.getTextSize()).setTextColor(colorValue).setLineHeight(180));
      richTextBuilder.addText(data.getContent());
      richTextBuilder.revertForm();
    }

    RichText richText = richTextBuilder.build();
    for (FormatData.PositionData data : positionDatas) {
      if (data.getType() == LinkType.LINK_TYPE) {
        richText.addTouchEventListener((component, touchEvent) -> {
          if (touchEvent.getAction() == TouchEvent.PRIMARY_POINT_UP) {
            if (linkClickListener != null) {
              linkClickListener.onLinkClickListener(LinkType.LINK_TYPE, data.getUrl(), null);
            } else {
              // 如果没有设置监听 则调用默认的打开浏览器显示连接
            }
            return false;
          }
          return true;
        }, data.getStart(), data.getEnd());
      }

      if (data.getType() == LinkType.MENTION_TYPE) {
        richText.addTouchEventListener((component, touchEvent) -> {
          if (touchEvent.getAction() == TouchEvent.PRIMARY_POINT_UP && linkClickListener != null) {
            linkClickListener.onLinkClickListener(LinkType.LINK_TYPE, data.getContent(), null);
            return false;
          }
          return true;
        }, data.getStart(), data.getEnd());
      }

      if (data.getType() == LinkType.SELF) {
        richText.addTouchEventListener((component, touchEvent) -> {
          if (touchEvent.getAction() == TouchEvent.PRIMARY_POINT_UP && linkClickListener != null) {
            linkClickListener.onLinkClickListener(LinkType.LINK_TYPE, data.getSelfAim(),
                data.getSelfContent());
            return false;
          }
          return true;
        }, data.getStart(), data.getEnd());
      }

      if (data.getType() == LinkType.OPEN) {
        richText.addTouchEventListener((component, touchEvent) -> {
          if (touchEvent.getAction() == TouchEvent.PRIMARY_POINT_UP) {
            if (needRealExpandOrContract) {
              if (mModel != null) {
                mModel.setStatus(StatusType.STATUS_CONTRACT);
                action(mModel.getStatus());
              } else {
                action();
              }
            }
            if (expandOrContractClickListener != null) {
              expandOrContractClickListener.onClick(StatusType.STATUS_EXPAND);
            }
            return false;
          }
          return true;
        }, data.getStart(), data.getEnd());
      }

      if (data.getType() == LinkType.CLOSE) {
        richText.addTouchEventListener((component, touchEvent) -> {
          if (touchEvent.getAction() == TouchEvent.PRIMARY_POINT_UP) {
            if (needRealExpandOrContract) {
              if (mModel != null) {
                mModel.setStatus(StatusType.STATUS_EXPAND);
                action(mModel.getStatus());
              } else {
                action();
              }
            }
            if (expandOrContractClickListener != null) {
              expandOrContractClickListener.onClick(StatusType.STATUS_CONTRACT);
            }
            return false;
          }
          return true;
        }, data.getStart(), data.getEnd());
      }
    }
    setRichText(richText);
  }

  /**
   * 获取需要插入的空格
   *
   * @param emptyWidth
   * @param endStringWidth
   * @return
   */
  private int getFitSpaceCount(float emptyWidth, float endStringWidth) {
    float measureText = mPaint.measureText(Space);
    int count = 0;
    while (endStringWidth + measureText * count < emptyWidth) {
      count++;
    }
    return --count;
  }

  // TODO: addSelf addMention addUrl

  /**
   * 设置当前的状态
   *
   * @param type
   */
  public void setCurrStatus(StatusType type) {
    action(type);
  }

  private void action() {
    action(null);
  }

  /**
   * 执行展开和收回的动作
   */
  private void action(StatusType type) {
    boolean isHide = currentLines < mLineCount;
    if (type != null) {
      mNeedAnimation = false;
    }
    if (mNeedAnimation) {
      AnimatorValue valueAnimator = new AnimatorValue();
      final boolean finalIsHide = isHide;
      valueAnimator.setValueUpdateListener((animatorValue, v) -> {
        if (finalIsHide) {
          currentLines = mLimitLines + (int) ((mLineCount - mLimitLines) * v);
        } else {
          if (mNeedContract)
            currentLines = mLimitLines + (int) ((mLineCount - mLimitLines) * (1 - v));
        }
        setRealContent(mContent);
      });
      valueAnimator.setDuration(100);
      valueAnimator.start();
    } else {
      if (isHide) {
        currentLines = mLimitLines + ((mLineCount - mLimitLines));
      } else {
        if (mNeedContract)
          currentLines = mLimitLines;
      }
      setRealContent(mContent);
    }
  }

  /**
   * 计算原内容被裁剪的长度
   *
   * @param endString
   * @param endPosition   指定行最后文字的位置
   * @param startPosition 指定行文字开始的位置
   * @param lineWidth     指定行文字的宽度
   * @param endStringWith 最后添加的文字的宽度
   * @param offset        偏移量
   * @return
   */
  private int getFitPosition(String endString, int endPosition, int startPosition, float lineWidth,
                             float endStringWith, float offset) {
    //最后一行需要添加的文字的字数
    int position =
        (int) ((lineWidth - (endStringWith + offset)) * (endPosition - startPosition) / lineWidth);

    if (position <= endString.length()) return endPosition;

    //计算最后一行需要显示的正文的长度
    String text = mFormatData.getFormatedContent();
    float measureText = mPaint.measureText(text.substring(startPosition, startPosition + position));

    //如果最后一行需要显示的正文的长度比最后一行的长减去“展开”文字的长度要短就可以了  否则加个空格继续算
    if (measureText <= lineWidth - endStringWith) {
      return startPosition + position;
    } else {
      return getFitPosition(endString, endPosition, startPosition, lineWidth, endStringWith,
          offset + mPaint.measureText(Space));
    }
  }


  /**
   * 对传入的数据进行正则匹配并处理
   *
   * @param content
   * @return
   */
  private FormatData formatData(CharSequence content) {
    FormatData formatData = new FormatData();
    List<FormatData.PositionData> datas = new ArrayList<>();
    Pattern pattern = Pattern.compile(self_regex, Pattern.CASE_INSENSITIVE);
    Matcher matcher = pattern.matcher(content);
    StringBuffer newResult = new StringBuffer();
    int start = 0;
    int end = 0;
    int temp = 0;

    // 对自定义的进行正则匹配
    if (this.mNeedSelf) {
      List<FormatData.PositionData> datasMention = new ArrayList<>();
      while (matcher.find()) {
        start = matcher.start();
        end = matcher.end();
        newResult.append(content.toString().substring(temp, start));
        //将匹配到的内容进行统计处理
        String result = matcher.group();
        //
        if (!(result == null || result.length() == 0)) {
          //解析数据
          String aimSrt = result.substring(result.indexOf("[") + 1, result.indexOf("]"));
          String contentSrt = result.substring(result.indexOf("(") + 1, result.indexOf(")"));
          String key = UUIDUtils.getUuid(result.length());
          datasMention.add(new FormatData.PositionData(newResult.length() + 1,
              newResult.length() + result.length(), aimSrt, aimSrt, contentSrt));
          newResult.append(key);
          temp = end;
        }
      }
      datas.addAll(datasMention);
    }

    newResult.append(content.toString().substring(end, content.toString().length()));
    content = newResult.toString();
    newResult = new StringBuffer();
    start = 0;
    end = 0;
    temp = 0;

    // 对链接 进行正则匹配
    if (mNeedLink) {
      pattern = Pattern.compile("(https?|ftp|file)://[-A-Za-z0-9+&@#/%?=~_|!:,.;" +
          "]+[-A-Za-z0-9+&@#/%=~|]", Pattern.CASE_INSENSITIVE);
      ;
      matcher = pattern.matcher(content);
      while (matcher.find()) {
        start = matcher.start();
        end = matcher.end();
        newResult.append(content.toString().substring(temp, start));
        if (mNeedConvertUrl) {
          //将匹配到的内容进行统计处理
          String result = matcher.group();
          String key = UUIDUtils.getUuid(result.length());
          datas.add(new FormatData.PositionData(newResult.length() + 1,
              newResult.length() + key.length(), TARGET, result));
          newResult.append(key);
        } else {
          String result = matcher.group();
          String key = UUIDUtils.getUuid(result.length());
          datas.add(new FormatData.PositionData(newResult.length() + 1,
              newResult.length() + key.length(), result, result));
          newResult.append(key);
        }
        temp = end;
      }
    }

    newResult.append(content.toString().substring(end, content.toString().length()));

    // 对@用户 进行正则匹配
    if (mNeedMention) {
      pattern = Pattern.compile(regexp_mention, Pattern.CASE_INSENSITIVE);
      matcher = pattern.matcher(newResult.toString());
      List<FormatData.PositionData> datasMention = new ArrayList<>();
      while (matcher.find()) {
        /// 将匹配到的内容进行统计处理
        datasMention.add(new FormatData.PositionData(matcher.start() + 1, matcher.end(),
            matcher.group(), matcher.group(), LinkType.MENTION_TYPE));
      }
      datas.addAll(0, datasMention);
    }

    // 按 getStart() 升序排列
    Collections.sort(datas, new Comparator<FormatData.PositionData>() {
      @Override
      public int compare(FormatData.PositionData p0, FormatData.PositionData p1) {
        if (p0.getStart() < p1.getStart()) {
          return -1;
        }
        if (p0.getStart() > p1.getStart()) {
          return 1;
        } else {
          return 0;
        }
      }
    });

    formatData.setPositionDatas(getAllPositionData(datas, newResult.toString()));
    StringBuffer str = new StringBuffer();
    for (FormatData.PositionData item : formatData.getPositionDatas()) {
      item.setStart(str.length());
      str.append(item.getContent());
      item.setEnd(str.length());
    }
    formatData.setFormatedContent(str.toString());
    return formatData;
  }

  private List<FormatData.PositionData> getAllPositionData(List<FormatData.PositionData> positionDatas, String content) {
    List<FormatData.PositionData> datas = new ArrayList<>();
    int start = 0;
    int end = 0;
    int temp = 0;
    for (FormatData.PositionData positionData : positionDatas) {

      if (positionData.getStart() > content.length() - 1 || positionData.getEnd() > content.length() - 1) {
        break;
      }

      if (start < positionData.getStart()) {
        end = positionData.getStart() - 1;
        datas.add(new FormatData.PositionData(start, end, content.substring(start, end)));
      }

      datas.add(positionData);
      start = positionData.getEnd();
    }

    if (end < content.length()) {
      end = content.length();
      datas.add(new FormatData.PositionData(start, end, content.substring(start, end)));
    }

    return datas;
  }

  /**
   * 绑定状态
   *
   * @param model
   */
  public void bind(ExpandableStatusFix model) {
    mModel = model;
  }

  // TODO: LocalLinkMovementMethod

  boolean dontConsumeNonUrlClicks = true;

  // TODO: Touch事件

  public interface OnLinkClickListener {
    void onLinkClickListener(LinkType type, String content, String selfContent);
  }

  public interface OnGetLineCountListener {
    /**
     * lineCount 预估可能占有的行数
     * canExpand 是否达到可以展开的条件
     */
    void onGetLineCount(int lineCount, boolean canExpand);
  }

  private OnGetLineCountListener onGetLineCountListener;

  public OnGetLineCountListener getOnGetLineCountListener() {
    return onGetLineCountListener;
  }

  public void setOnGetLineCountListener(OnGetLineCountListener onGetLineCountListener) {
    this.onGetLineCountListener = onGetLineCountListener;
  }

  public interface OnExpandOrContractClickListener {
    void onClick(StatusType type);
  }

  public OnLinkClickListener getLinkClickListener() {
    return linkClickListener;
  }

  public void setLinkClickListener(OnLinkClickListener linkClickListener) {
    this.linkClickListener = linkClickListener;
  }

  public boolean ismNeedMention() {
    return mNeedMention;
  }

  public void setNeedMention(boolean mNeedMention) {
    this.mNeedMention = mNeedMention;
  }

  public boolean isNeedContract() {
    return mNeedContract;
  }

  public void setNeedContract(boolean mNeedContract) {
    this.mNeedContract = mNeedContract;
  }

  public boolean isNeedExpend() {
    return mNeedExpend;
  }

  public void setNeedConvertUrl(boolean needConvertUrl) {
    mNeedConvertUrl = needConvertUrl;
  }

  public void setDefMaxLine(int line) {
    mLimitLines = line;
  }

  public void setNeedExpend(boolean mNeedExpend) {
    this.mNeedExpend = mNeedExpend;
  }

  public boolean isNeedAnimation() {
    return mNeedAnimation;
  }

  public void setNeedAnimation(boolean mNeedAnimation) {
    this.mNeedAnimation = mNeedAnimation;
  }

  public int getExpandableLineCount() {
    return mLineCount;
  }

  public void setExpandableLineCount(int mLineCount) {
    this.mLineCount = mLineCount;
  }

  public Color getExpandTextColor() {
    return mExpandTextColor;
  }

  public void setExpandTextColor(Color mExpandTextColor) {
    this.mExpandTextColor = mExpandTextColor;
  }

  public Color getExpandableLinkTextColor() {
    return mLinkTextColor;
  }

  public void setExpandableLinkTextColor(Color mLinkTextColor) {
    this.mLinkTextColor = mLinkTextColor;
  }

  public Color getContractTextColor() {
    return mContractTextColor;
  }

  public void setContractTextColor(Color mContractTextColor) {
    this.mContractTextColor = mContractTextColor;
  }

  public String getExpandString() {
    return mExpandString;
  }

  public void setExpandString(String mExpandString) {
    this.mExpandString = mExpandString;
  }

  public String getContractString() {
    return mContractString;
  }

  public void setContractString(String mContractString) {
    this.mContractString = mContractString;
  }

  public Color getEndExpandTextColor() {
    return mEndExpandTextColor;
  }

  public void setEndExpandTextColor(Color mEndExpandTextColor) {
    this.mEndExpandTextColor = mEndExpandTextColor;
  }

  public boolean isNeedLink() {
    return mNeedLink;
  }

  public void setNeedLink(boolean mNeedLink) {
    this.mNeedLink = mNeedLink;
  }

  public Color getSelfTextColor() {
    return mSelfTextColor;
  }

  public void setSelfTextColor(Color mSelfTextColor) {
    this.mSelfTextColor = mSelfTextColor;
  }

  public boolean isNeedSelf() {
    return mNeedSelf;
  }

  public void setNeedSelf(boolean mNeedSelf) {
    this.mNeedSelf = mNeedSelf;
  }

  public boolean isNeedAlwaysShowRight() {
    return mNeedAlwaysShowRight;
  }

  public void setNeedAlwaysShowRight(boolean mNeedAlwaysShowRight) {
    this.mNeedAlwaysShowRight = mNeedAlwaysShowRight;
  }

  public OnExpandOrContractClickListener getExpandOrContractClickListener() {
    return expandOrContractClickListener;
  }

  public void setExpandOrContractClickListener(OnExpandOrContractClickListener expandOrContractClickListener) {
    this.expandOrContractClickListener = expandOrContractClickListener;
  }

  public void setExpandOrContractClickListener(OnExpandOrContractClickListener expandOrContractClickListener, boolean needRealExpandOrContract) {
    this.expandOrContractClickListener = expandOrContractClickListener;
    this.needRealExpandOrContract = needRealExpandOrContract;
  }
}
