package com.ctetin.expandabletextviewlibrary.model;

import com.ctetin.expandabletextviewlibrary.app.StatusType;

/**
 * 为ExpandableTextView添加展开和收回状态的记录
 */
public interface ExpandableStatusFix {
  StatusType getStatus();

  void setStatus(StatusType type);
}
