package com.ctetin.expandabletextviewlibrary.app;

public enum StatusType {
  // 展开
  STATUS_EXPAND,
  // 收起
  STATUS_CONTRACT
}
