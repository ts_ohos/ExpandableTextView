package com.ctetin.expandabletextviewlibrary.app;

// 定义类型的枚举类型
public enum LinkType {
  DEFAULT,
  LINK_TYPE,
  MENTION_TYPE,
  SELF,
  OPEN,
  CLOSE,
  EndExpand,
}
