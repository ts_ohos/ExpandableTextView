package com.cretin.www.expandabletextview.slice;

import com.cretin.www.expandabletextview.ResourceTable;
import com.ctetin.expandabletextviewlibrary.ExpandableTextView;
import com.ctetin.expandabletextviewlibrary.app.LinkType;
import com.ctetin.expandabletextviewlibrary.app.StatusType;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.render.Paint;
import ohos.agp.text.RichText;
import ohos.agp.text.RichTextBuilder;
import ohos.agp.text.RichTextLayout;
import ohos.agp.text.TextForm;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.Rect;
import ohos.agp.utils.TextAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.multimodalinput.event.TouchEvent;

public class MainAbilitySlice extends AbilitySlice {
  private ExpandableTextView[] views;
  private Text[] tips;

  private Text tvTips00;

  private ExpandableTextView.OnLinkClickListener linkClickListener =
      (type, content, selfContent) -> {

        if (type.equals(LinkType.SELF)) {
          new ToastDialog(getContext()).setText("你点击了自定义规则 内容是：" + content).setAlignment(LayoutAlignment.BOTTOM).show();
        }

        if (type.equals(LinkType.MENTION_TYPE)) {
          new ToastDialog(getContext()).setText("你点击了@用户 内容是：" + content).setAlignment(LayoutAlignment.BOTTOM).show();
        }

        if (type.equals(LinkType.LINK_TYPE)) {
          new ToastDialog(getContext()).setText("你点击了链接 内容是：" + content).setAlignment(LayoutAlignment.BOTTOM).show();
        }
      };

  private void initView() {
    views = new ExpandableTextView[12];
    tips = new Text[11];
    views[0] = (ExpandableTextView) findComponentById(ResourceTable.Id_ep_01);
    views[1] = (ExpandableTextView) findComponentById(ResourceTable.Id_ep_02);
    views[2] = (ExpandableTextView) findComponentById(ResourceTable.Id_ep_03);
    views[3] = (ExpandableTextView) findComponentById(ResourceTable.Id_ep_04);
    views[4] = (ExpandableTextView) findComponentById(ResourceTable.Id_ep_05);
    views[5] = (ExpandableTextView) findComponentById(ResourceTable.Id_ep_06);
    views[6] = (ExpandableTextView) findComponentById(ResourceTable.Id_ep_07);
    views[7] = (ExpandableTextView) findComponentById(ResourceTable.Id_ep_08);
    views[8] = (ExpandableTextView) findComponentById(ResourceTable.Id_ep_09);
    views[9] = (ExpandableTextView) findComponentById(ResourceTable.Id_ep_10);
    views[10] = (ExpandableTextView) findComponentById(ResourceTable.Id_ep_11);
    views[11] = (ExpandableTextView) findComponentById(ResourceTable.Id_ep_12);
    tips[0] = (Text) findComponentById(ResourceTable.Id_tv_tips01);
    tips[1] = (Text) findComponentById(ResourceTable.Id_tv_tips02);
    tips[2] = (Text) findComponentById(ResourceTable.Id_tv_tips03);
    tips[3] = (Text) findComponentById(ResourceTable.Id_tv_tips04);
    tips[4] = (Text) findComponentById(ResourceTable.Id_tv_tips05);
    tips[5] = (Text) findComponentById(ResourceTable.Id_tv_tips06);
    tips[6] = (Text) findComponentById(ResourceTable.Id_tv_tips07);
    tips[7] = (Text) findComponentById(ResourceTable.Id_tv_tips08);
    tips[8] = (Text) findComponentById(ResourceTable.Id_tv_tips09);
    tips[9] = (Text) findComponentById(ResourceTable.Id_tv_tips10);
    tvTips00 = (Text) findComponentById(ResourceTable.Id_tv_tips00);
  }

  @Override
  public void onStart(Intent intent) {
    super.onStart(intent);
    super.setUIContent(ResourceTable.Layout_ability_main);

    initView();

    setTips();

    String yourText = "    我所认识的中国，http://www.baidu.com 强大、友好 --习大大。@奥特曼 " +
        "“一带一路”经济带带动了沿线国家的经济发展，促进我国与他国的友好往来和贸易发展，可谓“双赢”，Github地址。 " +
        "自古以来，中国以和平、友好的面孔示人。汉武帝派张骞出使西域，开辟丝绸之路，增进与西域各国的友好往来。http://www.baidu.com " +
        "胡麻、胡豆、香料等食材也随之传入中国，汇集于中华美食。@RNG 漠漠古道，驼铃阵阵，这条路奠定了“一带一路”的基础，让世界认识了中国。";
    setContent(yourText, true);

    // 在RecyclerView中查看效果
    findComponentById(ResourceTable.Id_ll_recyclerview).setClickedListener(component -> {
      present(new ShowInRecyclerViewAbilitySlice(), new Intent());
    });
  }

  @Override
  public void onActive() {
    super.onActive();
  }

  @Override
  public void onForeground(Intent intent) {
    super.onForeground(intent);
  }

  /**
   * 设置内容
   *
   * @param yourText
   * @param d
   */
  private void setContent(String yourText, boolean d) {
    //1、正常带链接和@用户，没有展开和收回功能
    views[0].setContent(yourText);
    views[0].setLinkClickListener(linkClickListener);

    //2、正常带链接，不带@用户，有展开和收回功能，有切换动画
    views[1].setContent(yourText);
    views[1].setLinkClickListener(linkClickListener);
    views[11].setContent(yourText);
    views[11].setLinkClickListener(linkClickListener);
    //添加展开和收回操作
    views[1].setExpandOrContractClickListener(type -> {
      if (type.equals(StatusType.STATUS_CONTRACT)) {
        new ToastDialog(getContext()).setText("收回操作").setAlignment(LayoutAlignment.CENTER).show();
      } else {
        new ToastDialog(getContext()).setText("展开操作").setAlignment(LayoutAlignment.CENTER).show();
      }
    });
    //添加展开和收回操作 只触发点击 不真正触发展开和收回操作
    views[11].setExpandOrContractClickListener(type -> {
      if (type.equals(StatusType.STATUS_CONTRACT)) {
        new ToastDialog(getContext()).setText("收回操作，不真正触发收回操作").setAlignment(LayoutAlignment.CENTER).show();
      } else {
        new ToastDialog(getContext()).setText("展开操作，不真正触发展开操作").setAlignment(LayoutAlignment.CENTER).show();
      }
    }, false);

    //3、正常不带链接，不带@用户，有展开和收回功能，有切换动画
    views[2].setContent(yourText);
    views[2].setLinkClickListener(linkClickListener);

    //4、正常带链接和@用户，有展开和收回功能，有切换动画
    views[3].setContent(yourText);
    views[3].setLinkClickListener(linkClickListener);
    views[3].setNeedSelf(true);

    //5、正常带链接和@用户，有展开和收回功能，没有切换动画
    views[4].setContent(yourText);
    views[4].setLinkClickListener(linkClickListener);

    //6、正常带链接和@用户，有展开，没有收回功能
    views[5].setContent(yourText);
    views[5].setLinkClickListener(linkClickListener);

    //7、正常带链接和@用户，有展开，有收回功能，带附加内容(比如时间)
    views[6].setContent(yourText);
    views[6].setEndExpendContent(" 1小时前");
    views[6].setLinkClickListener(linkClickListener);

    //8、正常带链接和@用户，有展开，没有收回功能，带附加内容(比如时间)
    views[7].setContent(yourText);
    views[7].setEndExpendContent(" 1小时前");
    views[7].setLinkClickListener(linkClickListener);

    //9、正常带链接和@用户，有展开，有收回功能，有'展开'和'收起'始终靠右显示的功能
    views[8].setContent(yourText);
    views[8].setLinkClickListener(linkClickListener);
    //需要先开启始终靠右显示的功能
    views[8].setNeedAlwaysShowRight(true);

    //10、正常带链接和@用户，有展开，有收回功能，带自定义规则（解析[标题](规则)并处理，例如对一些字段进行自定义处理，比如文字中的"--习大大" 和 "Gitbub地址"）
    //如果你需要对一些字段进行自定义处理，比如文字中的"--习大大" 和 "Gitbub地址"，你需要按照下面的形式去组装数据，这样控件就可以自动去解析并展示。
    /**
     * 如需使用此功能，需要先开启
     * app:ep_need_self="true"
     * views[8].setNeedSelf(true);
     */
    String yourText1 = "";
    if (d) {
      yourText1 = "    我所认识的中国，强大、友好，[--习大大](schema_jump_userinfo)。[http://www.baidu.com]" +
          "(http://www.baidu.com)，@奥特曼 “一带一路”经济带带动了沿线国家的经济发展，促进我国与他国的友好往来和贸易发展，可谓“双赢”，[Github地址]" +
          "(https://github.com/MZCretin/ExpandableTextView)。http://www.baidu.com " +
          "自古以来，中国以和平、友好的面孔示人。汉武帝派张骞出使西域，开辟丝绸之路，增进与西域各国的友好往来。http://www.baidu.com " +
          "胡麻、胡豆、香料等食材也随之传入中国，汇集于中华美食。@RNG 漠漠古道，驼铃阵阵，这条路奠定了“一带一路”的基础，让世界认识了中国。";
    } else {
      tips[9].setText("10、正常带链接和@用户，有展开，有收回功能，带自定义规则");
      yourText1 = yourText;
      setTips();
    }
    views[9].setContent(yourText1);
    views[9].setLinkClickListener(linkClickListener);
    //需要先开启
    views[9].setNeedSelf(true);
    views[9].setNeedAlwaysShowRight(true);

    //11、正常带链接和@用户，有展开，有收回功能，文本中链接不转换成网页链接的文本提示
    views[10].setContent(yourText);
    views[10].setLinkClickListener(linkClickListener);
    //监听是否初始化完成 在这里可以获取是否支持展开/收回
    views[10].setOnGetLineCountListener(new ExpandableTextView.OnGetLineCountListener() {
      @Override
      public void onGetLineCount(int lineCount, boolean canExpand) {
        new ToastDialog(getContext()).setText("行数：" + lineCount + "  是否满足展开条件：" + canExpand).setAlignment(LayoutAlignment.CENTER).show();
      }
    });
  }

  /*设置Tips*/
  private void setTips() {
    Text view = tvTips00;
    RichTextBuilder builder = new RichTextBuilder();
    builder.mergeForm(new TextForm().setLineHeight(300).setTextSize(view.getTextSize()).setTextColor(view.getTextColor().getValue()));
    builder.addText("ExpandTextView包含如下功能：\n " +
        "\n1、实现类似微博@好友的功能，对@内容进行高亮显示，可以点击\n2、实现链接的高亮显示，可以点击，并且对链接进行原文隐藏\n3" +
        "、实现内容超过指定行数折叠效果，点击可以展开内容\n4、在原文内容末尾添加指定内容，比如时间串，详情见效果图\n5、自定义高亮规则，实现自定义位置点击\n6" +
        "、RecyclerView中可记住展开和收回状态\n7、可固定展开和收回按钮在最右边或紧贴内容后边\n \n");
    builder.addText("联系我:\n");
    builder.mergeForm(new TextForm().setLineHeight(300).setTextSize(view.getTextSize()).setTextColor(0xFFFF0080).setUnderline(true));
    builder.addText("792075058@qq.com\n");
    builder.revertForm();
    builder.addText(" \n博客地址：\n");
    builder.mergeForm(new TextForm().setLineHeight(300).setTextSize(view.getTextSize()).setTextColor(0xFFFF0080).setUnderline(true));
    builder.addText("https://www.jianshu.com/p/5519fbab6907\n");
    builder.revertForm();
    builder.addText(" \nGithub地址(您的star是对我最大的鼓励)：\n");
    builder.mergeForm(new TextForm().setLineHeight(300).setTextSize(view.getTextSize()).setTextColor(0xFFFF0080).setUnderline(true));
    builder.addText("https://github.com/MZCretin/ExpandableTextView\n");
    builder.revertForm();
    builder.addText(" \n");

    final RichText richTextContent = builder.build();
    view.setRichText(richTextContent);
  }
}
