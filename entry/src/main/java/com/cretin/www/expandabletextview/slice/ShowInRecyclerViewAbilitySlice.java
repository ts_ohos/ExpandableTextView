package com.cretin.www.expandabletextview.slice;

import com.cretin.www.expandabletextview.ResourceTable;
import com.cretin.www.expandabletextview.model.ViewModel;
import com.cretin.www.expandabletextview.model.ViewModelWithFlag;
import com.ctetin.expandabletextviewlibrary.ExpandableTextView;
import com.ctetin.expandabletextviewlibrary.app.LinkType;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

public class ShowInRecyclerViewAbilitySlice extends AbilitySlice {

  private ListContainer recyclerView;
  private MyRecyclerViewAdapter adapter;
  private List list;

  //是需要保存展开或收回状态
  private boolean flag = true;

  private static final String yourText = "我所认识的中国，强大、友好我所认识的中国，强大、友好。@奥特曼 " +
      "“一带一路”经济带带动了沿线国家的经济发展，促进我国与他国的友好往来和贸易发展，可谓“双赢”。http://www.baidu.com " +
      "自古以来，中国以和平、友好的面孔示人。汉武帝派张骞出使西域，开辟丝绸之路，增进与西域各国的友好往来。http://www.baidu.com " +
      "胡麻、胡豆、香料等食材也随之传入中国，汇集于中华美食。@RNG 漠漠古道，驼铃阵阵，这条路奠定了“一带一路”的基础，让世界认识了中国。";


  @Override
  protected void onStart(Intent intent) {
    super.onStart(intent);
    super.setUIContent(ResourceTable.Layout_ability_show_in_recycler_view);

    recyclerView = (ListContainer) findComponentById(ResourceTable.Id_recyclerview);
    // floatingActionButton = findViewById(R.id.float_btn);

    list = new ArrayList<>();
    changeStateAndSetData(null, false);

//        floatingActionButton.setOnClickListener(v -> changeStateAndSetData(content,true));
  }

  /**
   * 切换状态
   */
  private void changeStateAndSetData(String content, boolean change) {
    if (change) flag = !flag;
    list.clear();
    if (flag) {
      new ToastDialog(getContext()).setText("保留之前的展开或收回状态").setAlignment(LayoutAlignment.CENTER).show();
      for (int i = 0; i < 50; i++) {
        if (content == null || content.length() == 0)
          list.add(new ViewModelWithFlag("第" + (i + 1) + "条数据：" + yourText));
        else
          list.add(new ViewModelWithFlag("第" + (i + 1) + "条数据：" + content));
      }
    } else {
      new ToastDialog(getContext()).setText("不保留之前的展开或收回状态").setAlignment(LayoutAlignment.CENTER).show();
      for (int i = 0; i < 50; i++) {
        if (content == null || content.length() == 0)
          list.add(new ViewModel("第" + (i + 1) + "条数据：" + yourText));
        else
          list.add(new ViewModel("第" + (i + 1) + "条数据：" + content));
      }
    }
    adapter = new MyRecyclerViewAdapter(getContext(), list);
    recyclerView.setItemProvider(adapter);

    recyclerView.setBindStateChangedListener(new Component.BindStateChangedListener() {
      @Override
      public void onComponentBoundToWindow(Component component) {
        adapter.notifyDataChanged();
      }

      @Override
      public void onComponentUnboundFromWindow(Component component) {

      }
    });
  }

  @Override
  protected void onForeground(Intent intent) {
    super.onForeground(intent);
    adapter.notifyDataChanged();
  }

  public class MyRecyclerViewAdapter extends BaseItemProvider {

    private final Context mContext;
    private final List dataList;

    MyRecyclerViewAdapter(Context context, List list) {
      this.mContext = context;
      this.dataList = list;
    }

    @Override
    public int getCount() {
      return dataList.size();
    }

    @Override
    public Object getItem(int i) {
      if (list != null && i >= 0 && i < list.size()) {
        return list.get(i);
      }
      return null;
    }

    @Override
    public long getItemId(int i) {
      return i;
    }

    @Override
    public Component getComponent(int i, Component component,
                                  ComponentContainer componentContainer) {
      final Component cpt;
      if (component == null) {
        cpt = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_item_text, null,
            false);
      } else {
        cpt = component;
      }
      cpt.setClickedListener(listener -> {
        // 点击
      });

      ExpandableTextView text =
          (ExpandableTextView) cpt.findComponentById(ResourceTable.Id_tv_item);
      if (flag) {
        // 注意：保留状态需要在设置内容之前调用bind方法
        text.bind((ViewModelWithFlag) list.get(i));
        text.setContent(((ViewModelWithFlag) list.get(i)).getTitle());
      } else {
        text.setContent(((ViewModel) list.get(i)).getTitle());
      }
      text.setLinkClickListener((type, content, selfContent) -> {
        if (type.equals(LinkType.SELF)) {
          new ToastDialog(getContext()).setText("你点击了自定义规则 内容是：" + content).setAlignment(LayoutAlignment.BOTTOM).show();
        }

        if (type.equals(LinkType.MENTION_TYPE)) {
          new ToastDialog(getContext()).setText("你点击了@用户 内容是：" + content).setAlignment(LayoutAlignment.BOTTOM).show();
        }

        if (type.equals(LinkType.LINK_TYPE)) {
          new ToastDialog(getContext()).setText("你点击了链接 内容是：" + content).setAlignment(LayoutAlignment.BOTTOM).show();
        }
      });
      text.setExpandOrContractClickListener(type -> {
        notifyDataChanged();
      });

      return cpt;
    }
  }

}
