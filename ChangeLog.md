# Changelog
本组件基于原库master v1.6.1 版本移植

### v.1.0.0
移植OHOS后首次上传

support
    ExpandTextView是多行文本缩略组件，主要实现功能如下：
    (1)类似微博@好友功能，对@内容进行高亮显示，可点击;
    (2)实现链接的高亮显示，可以点击，并且对链接进行原文隐藏;
    (3)实现内容超过指定行数折叠效果，点击可以展开内容;
    (4)在原文内容未尾添加指定内容，比如时间串；
    (5)自定义高亮规则，实现自定义位置点击
    (6)列表组件中可记住展开和收回状态；
    可固定展开和回收按钮在最右边或紧贴内容后边
    注：p40虚拟机无法正常显示。真机和虚拟机mate30可以正常显示

not support
设置链接图标: ohos暂无此类api

+ 2019-07-04 12:06:06更新，如果你需要监听展开和回收的时间监听，但是不需要控件真正的执行展开和回收操作，你可以在添加展开和收回操作的时候置顶是否需要真正执行展开和回收操作，具体效果可以参考效果图第2条的第二个，依赖版本请使用tag版本v1.6.1,[查看说明](#v1612019-07-04-120205-更新了如下特性-版本v161可以正常使用)

  ```java
  implementation 'com.github.MZCretin:ExpandableTextView:v1.6.1'
  ```

+ 2019-05-20 15:14:04更新，如果你需要展示链接但是不想让链接自动转换成"网页链接"的形式，你可以禁用自动转换功能；如果你希望知道是否满足展开/收起的条件，添加一个监听就好了，依赖版本请使用tag版本1.6.0，[查看说明](#v162019-05-20-151910-更新了如下特性-版本v16可以正常使用)

  ```java
  implementation 'com.github.MZCretin:ExpandableTextView:v1.6.0'
  ```

+ 2019-03-14 10:25:57更新，修复在有些手机上偶尔会出现白屏，加载不出内容的情况，依赖版本请使用tag版本1.5.3

  ```java
  implementation 'com.github.MZCretin:ExpandableTextView:v1.5.3'
  ```

+ 2018-10-09 17:20:45 更新，新增对展开和回收的点击事件监听，依赖版本请使用tag版本v1.5.2

    ```java
    implementation 'com.github.MZCretin:ExpandableTextView:v1.5.2'
    ```

+ 2018-09-28 09:37:28 更新，优化了将"展开"和"收回"固定最右显示时中间空格数量的计算方式，依赖版本请使用tag版本v1.5.1，[查看说明](#新特性额外说明)

    ```java
    implementation 'com.github.MZCretin:ExpandableTextView:v1.5.1'
    ```

+ 2018-09-27 09:18:14 更新

    + 修复了不添加事件监听，点击链接会直接打开百度页面；
    + 在demo中添加自定义设置显示文本的功能，您可以自己设置需要显示的文本，然后查看对应的显示效果；
    + 新增了"展开"和"收回"按钮始终居右的功能，具体效果请查看效果图的第9条，依赖版本请使用tag版本v1.5，[查看说明](#新特性额外说明)

    ```java
    implementation 'com.github.MZCretin:ExpandableTextView:v1.5'
    ```

+ 2018-09-22 23:32:16 更新，新增自定义规则解析，具体效果请查看效果图的第10条，依赖版本请使用tag版本v1.4，[查看说明](#新特性额外说明)

    ```java
    implementation 'com.github.MZCretin:ExpandableTextView:v1.4'
    ```

+ 2018-09-21 11:51:24 更新，优化了demo的代码逻辑和注释

+ 2018-09-21 08:45:13 更新，修复了自定义设置展开和收回内容无效的问题，依赖请使用tag版本v1.3.1
    ```java
    implementation 'com.github.MZCretin:ExpandableTextView:v1.3.1'
    ```

+ 2018-09-20 16:31:13 更新
    + 一、提供了在RecyclerView中使用的时候，对之前状态的保存的功能支持，[查看说明](#新特性额外说明)；
    + 二、新增对@用户和链接的处理，用户可以设置不对这些内容进行识别，仅仅使用展开和收回功能；
    + 三、优化的demo的效果，请大家重新下载apk进行体验。
    + 四、如果你没有设置对链接的监听，会默认调用系统浏览器打开链接
    + 五、支持语言国际化
    + 六、最新版请使用v1.3

+ 2018-09-03 17:39:56 修复一些bug，链接sheSpan位置错误，未生成release，等待下次修复其他bug一起打tag依赖包，使用请本地依赖使用

+ 2018-08-31 17:31:56 优化设置padding对宽度造成的影响，依赖请使用tag版本v1.2
    ```java
    implementation 'com.github.MZCretin:ExpandableTextView:v1.2'
    ```

+ 2018-08-31 11:21:22 在V1.0的基础上进行了优化，依赖请使用tag版本v1.1

    ```java
    implementation 'com.github.MZCretin:ExpandableTextView:v1.1'
    ```
+ 2021-08-20 在V1.0.0 的基础上进行了移植OHOS
    项目移植状态：部分移植
            未移植部分：
                设置链接图标: ohos暂无此类api