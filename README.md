# ExpandableTextView
    扩展TextView，使其可以展开和点击跳转
本项目是基于开源项目ExpandableTextView进行harmonyos化的移植和开发的，可以通过项目标签以及
[github地址](https://github.com/MZCretin/ExpandableTextView)

移植版本：源master v1.6.1版本

## 项目介绍
### 项目名称：ExpandableTextView
### 所属系列：harmonyos的第三方组件适配移植
### 功能：
    ExpandTextView是多行文本缩略组件，主要实现功能如下：
    (1)类似微博@好友功能，对@内容进行高亮显示，可点击;
    (2)实现链接的高亮显示，可以点击，并且对链接进行原文隐藏;
    (3)实现内容超过指定行数折叠效果，点击可以展开内容;
    (4)在原文内容未尾添加指定内容，比如时间串；
    (5)自定义高亮规则，实现自定义位置点击
    (6)列表组件中可记住展开和收回状态；
    可固定展开和回收按钮在最右边或紧贴内容后边
    注：p40虚拟机无法正常显示。真机和虚拟机mate30可以正常显示

### 项目移植状态：部分移植
        未移植部分：
            设置链接图标: ohos暂无此类api
### 调用差异：基本没有使用差异，请参照demo使用
### 原项目Doc地址：https://github.com/MZCretin/ExpandableTextView
### 编程语言：java

### 项目截图（涉及文件仅供demo测试使用）

![demo运行效果](art/art.jpg)
![运行效果](art/demo.gif)

## 安装教程

#### 方案一  
 
可以先下载项目，将项目中的expandabletextviewlibrary库提取出来放在所需项目中通过build配置
```Java
dependencies {
     implementation project(":expandabletextviewlibrary")
}
```

#### 方案二

- 1.项目根目录的build.gradle中的repositories添加：
```groovy
    buildscript {
       repositories {
           ...
           mavenCentral()
       }
       ...
   }
   
   allprojects {
       repositories {
           ...
           mavenCentral()
       }
   }
```
- 2.开发者在自己的项目中添加依赖
```groovy
dependencies {
    implementation 'com.gitee.ts_ohos:expandabletextviewlibrary:1.0.0'
}
```

# How to use
    
    <com.ctetin.expandabletextviewlibrary.ExpandableTextView
                ohos:id="$+id:ep_09"
                ohos:height="match_content"
                ohos:width="match_parent"
                ohos:margin="15vp"
                ohos:additional_line_spacing="4vp"
                ohos:multiple_lines="true"
                ohos:text_size="14fp"
                ohos:text_color="#333333"
                app:ep_max_line="5"
                app:ep_need_contract="true"
                app:ep_need_expand="true"
                app:ep_need_self="true" />
                
    // 需要先开启始终靠右显示的功能
    views[8].setNeedAlwaysShowRight(true);
    
    // 点击高亮点击事件
    views[8].setLinkClickListener(linkClickListener);
    
    //监听是否初始化完成 在这里可以获取是否支持展开/收回
    views[10].setOnGetLineCountListener(new ExpandableTextView.OnGetLineCountListener() {
        @Override
        public void onGetLineCount(int lineCount, boolean canExpand) {
            new ToastDialog(getContext()).setText("行数：" + lineCount + "  是否满足展开条件：" + canExpand).setAlignment(LayoutAlignment.CENTER).show();
        }
    });
            


